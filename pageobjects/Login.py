from selenium.webdriver import ActionChains
from selenium.webdriver.support.wait import WebDriverWait

from features.environment import before_scenario
from utils.browsers import browsers
from .common.basepage import BASEPAGE
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

from utils.general import *
import time
import os
from utils.paths import project_path


class Login(BASEPAGE):
    locator_dictionary = {
        "user_name": (By.ID, 'username'),
        "password": (By.ID, 'password'),
        "login_btn": (By.ID, 'Login'),
        "register_btn": (By.XPATH, './/button[@title = " Create New Profile"]'),
        "close_btns": (By.XPATH, './/button[@title="Close TEST PANOSHARPIE"]')
    }

    constants = {
        "link": '/lightning/page/home/',
        "clinician": '/lightning/n/BCH_In_Clinic_Experience_Home'
    }

    def go_to(self):
        base_url = get_setting("URL", "sf_org")
        self.browser.get(base_url + self.constants["link"])
        # self.browser.get(base_url)
        try:
            WebDriverWait(self.browser, self.WAIT).until(
                EC.presence_of_element_located(self.locator_dictionary["login_btn"]))
        except:
            print("Exception: The user is not navigated to Login screen.")
        # self.click_element(self.find_element(self.locator_dictionary["atc_btn"]))
        var = self.find_element(self.locator_dictionary["login_btn"])
        assert var is not None, "The user is not navigated to Login screen."

    def login_into_website(self, user_name, password, home_screen):
        self.send_text_to_element(self.find_element(self.locator_dictionary["user_name"]), user_name)
        self.send_text_to_element(self.find_element(self.locator_dictionary["password"]), password)

        self.click_element(self.find_element(self.locator_dictionary["login_btn"]))
        self.is_log_in("no")

    def is_log_in(self, already_log_in):
        if already_log_in == "yes":
            base_url = get_setting("URL", "sf_org")
            self.browser.get(base_url + self.constants["link"])
        try:
            WebDriverWait(self.browser, 60).until(
                EC.presence_of_element_located(self.locator_dictionary["register_btn"]))
        except:
            print("Exception: The user is not navigated to Home screen.")

        close_btns = self.find_elements(self.locator_dictionary["close_btns"])
        print("Total opened tab: " + str(len(close_btns)))
        for button in close_btns:
            self.click_element(button)
            time.sleep(3)
        # self.click_element(self.find_element(self.locator_dictionary["atc_btn"]))
        var = self.find_element(self.locator_dictionary["register_btn"])
        assert var is not None, "The user is not navigated to Home screen."

    def close_browser(self, user_type):
        if user_type == "call_center_agent":
            self.browser.quit()
            default_browser = get_browser_name()
            self.browser = browsers(self, get_config().userdata.get("browser", default_browser))