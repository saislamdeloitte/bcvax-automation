from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait

from .common.basepage import BASEPAGE
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

from utils.general import *
import time


class ClinicalPrecess(BASEPAGE):
    locator_dictionary = {
        "nav_button": (By.XPATH, './/div[@role="navigation"]/one-app-launcher-header/button'),
        "nav_search_bar": (By.XPATH, '(.//input[@type="search"])[2]'),
        "hospital_field": (By.XPATH, '(.//input[contains(@class, "slds-input")])[5]'),
        "reg_number_field": (By.XPATH, '(.//input[contains(@class, "slds-input")])[4]'),
        "search_btn": (By.XPATH , './/button[text() = "Search"]'),
        "view_btn": (By.CSS_SELECTOR, 'button[title="View Case"]'),
        "current_step": (By.XPATH, './/div[contains(@class, "current-step")]/h2'),
        "edit_icon": (By.XPATH, '(.//lightning-icon[contains(@class,"slds-icon-utility-edit")])[2]'),

        "address_field": (By.CSS_SELECTOR, 'input[name="DDH__HC_Address_1__c"]'),
        "city_field": (By.CSS_SELECTOR, 'input[name="DDH__HC_City__c"]'),
        "state_field": (By.CSS_SELECTOR, 'input[name="DDH__HC_State__c"]'),
        "zip_code_field": (By.CSS_SELECTOR, 'input[name="DDH__HC_Zip_Code__c"]'),
        "submit_btn": (By.XPATH, './/button[text() = "Submit"]'),
        "edit_success": (By.XPATH, './/div[text() = "Information updated"]'),
        "rebook_btn": (By.XPATH, './/button[text() = "Rebook at Current Location"]'),
        "rebook_success": (By.XPATH, './/span[text() = "Appointment is successfully booked"]'),
        "confirm_btn_1": (By.XPATH, './/button[text() = "Confirm & Save Identification"]'), # information_updated, current_step
        "return_btn": (By.XPATH, './/button[text() = "Return to Search"]'),

        "related_tab_id": (By.XPATH, 'relatedListsTab__item'),
        "immunization_no": (By.XPATH, '(.//h3/lst-template-list-field/force-lookup/div/force-hoverable-link/div/a/sp'
                                      'an)[1]')


    }

    constants = {
        "in_clinic": '/lightning/n/BCH_In_Clinic_Experience_Home',
        "clinic_in_box": '/lightning/page/home'
    }

    def go_to(self):
        base_url = get_setting("URL", "sf_org")
        self.browser.get(base_url + self.constants["link"])
        # self.browser.get(base_url)
        try:
            WebDriverWait(self.browser, self.WAIT).until(
                EC.presence_of_element_located(self.locator_dictionary["login_btn"]))
        except:
            print("Exception: The user is not navigated to Login screen.")
        # self.click_element(self.find_element(self.locator_dictionary["atc_btn"]))
        var = self.find_element(self.locator_dictionary["login_btn"])
        assert var is not None, "The user is not navigated to Login screen."

    def login_into_website(self, user_name, password, home_screen):
        self.send_text_to_element(self.find_element(self.locator_dictionary["user_name"]), user_name)
        self.send_text_to_element(self.find_element(self.locator_dictionary["password"]), password)

        self.click_element(self.find_element(self.locator_dictionary["login_btn"]))
        try:
            WebDriverWait(self.browser, 60).until(
                EC.presence_of_element_located(self.locator_dictionary["register_btn"]))
        except:
            print("Exception: The user is not navigated to Home screen.")
        # self.click_element(self.find_element(self.locator_dictionary["atc_btn"]))
        var = self.find_element(self.locator_dictionary["register_btn"])
        assert var is not None, "The user is not navigated to Home screen."

    def move_to(self, module_name):
        e = None
        self.click_element(self.find_element(self.locator_dictionary["nav_button"]))
        self.send_text_to_element(self.find_element(self.locator_dictionary["nav_search_bar"]), module_name)
        self.send_text_to_element(self.find_element(self.locator_dictionary["nav_search_bar"]), Keys.ENTER)
        try:
            e = WebDriverWait(self.browser, 20).until(
                EC.presence_of_element_located(self.locator_dictionary["reg_number_field"]))
        except:
            print("Exception: The user is not navigated to" + module_name + " screen.")
        assert e is not None, "The user is not navigated to " + module_name + "screen."

    def search_citizen(self, reg_no):
        e = None
        self.send_text_to_element(self.find_element(self.locator_dictionary["reg_number_field"]), reg_no)
        self.find_element(self.locator_dictionary["hospital_field"]).clear()
        self.click_element(self.find_element(self.locator_dictionary["search_btn"]))

        try:
            e = WebDriverWait(self.browser, 20).until(
                EC.presence_of_element_located(self.locator_dictionary["view_btn"]))
        except:
            print("Exception")
        assert e is not None, "The searched records are not present."

    def view_record(self, process_screen):
        e = None
        self.click_element(self.find_element(self.locator_dictionary["view_btn"]))

        try:
            e = WebDriverWait(self.browser, 20).until(
                EC.presence_of_element_located(self.locator_dictionary["current_step"]))
        except:
            print("Exception")
        assert self.get_element_text(e) == process_screen, "The " + process_screen + " process is in wrong stage."

    def edit_record(self):
        e = None
        self.click_element(self.find_element(self.locator_dictionary["edit_icon"]))

        e = WebDriverWait(self.browser, 20).until(
            EC.presence_of_element_located(self.locator_dictionary["address_field"]))

        address_field = self.get_attribute(self.find_element(self.locator_dictionary["address_field"]), "value")
        if address_field == "":
            self.send_text_to_element(self.find_element(self.locator_dictionary["address_field"]), "8801 SECOND AVE")

        city_field = self.get_attribute(self.find_element(self.locator_dictionary["city_field"]), "value")
        if city_field == "":
            self.send_text_to_element(self.find_element(self.locator_dictionary["city_field"]), "VANCOUVER")

        state_field = self.get_attribute(self.find_element(self.locator_dictionary["state_field"]), "value")
        if state_field == "":
            self.send_text_to_element(self.find_element(self.locator_dictionary["state_field"]), "BC")

        zip_code_field = self.get_attribute(self.find_element(self.locator_dictionary["zip_code_field"]), "value")
        if zip_code_field == "":
            self.send_text_to_element(self.find_element(self.locator_dictionary["zip_code_field"]), "V0R2Y0")

        self.click_element(self.find_element(self.locator_dictionary["submit_btn"]))

        try:
            e = WebDriverWait(self.browser, 20).until(
                EC.presence_of_element_located(self.locator_dictionary["edit_success"]))
        except:
            print("Exception")
        assert e is not None, "The fields are not updated."

    def rebook_appointment(self, rebook_btn):
        e = None
        self.click_element(self.find_element(self.locator_dictionary["rebook_btn"]))

        try:
            e = WebDriverWait(self.browser, 20).until(
                EC.presence_of_element_located(self.locator_dictionary["rebook_success"]))
        except:
            print("Exception")
        assert e is not None, "The appointment re-booking failed."

    def confirm_step(self):
        e = None
        self.click_element(self.find_element(self.locator_dictionary["confirm_btn_1"]))

        try:
            e = WebDriverWait(self.browser, 20).until(
                EC.presence_of_element_located(self.locator_dictionary["edit_success"]))
        except:
            print("Exception")
        assert e is not None, "The appointment confirmation failed."
