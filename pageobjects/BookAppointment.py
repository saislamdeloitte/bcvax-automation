from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait

from .common.basepage import BASEPAGE
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

from utils.general import *
import time


class BookAppointment(BASEPAGE):
    locator_dictionary = {
        "email_field": (By.ID, 'identifierId'),
        "next_btn": (By.XPATH, '//*[@id="identifierNext"]/div/button'),
        "password_field": (By.CSS_SELECTOR, 'input[name=password]'),
        "password_next_btn": (By.XPATH, '//*[@id="passwordNext"]/div/button'),
        "search_mail": (By.CSS_SELECTOR, 'input[placeholder="Search mail"]'),
        "search_button": (By.CSS_SELECTOR, 'button[aria-label="Search mail"]'),
        "iframe_to_be_located": (By.ID, 'gtn-roster-iframe-id'),
        "first_email": (By.XPATH,
                        './/table/tbody/tr/td[5]/div[1]/div/div[3]/span/span[@class="bqe"][contains(text(), "Your Booking Link")]'),
        "click_here_link": (
        By.XPATH, './/a[contains(@href , "https://bcvaxuat-citizenportal.cs148.force.com/s/booking")]'),
        "booking_email": (By.XPATH,
                          './/table/tbody/tr/td[5]/div[1]/div/div[3]/span/span[@class="bqe"][contains(text(), "You\'re Booked:")]'),
        "booking_ui_widget": (By.XPATH, './/div[text()="COVID-19 Immunization"]'),
        "reg_no_field": (By.XPATH, '//*[@id="input-6"]'),
        "phn_number_field": (By.XPATH, '//*[@id="input-10"]'),
        "book_button": (By.XPATH, './/button[text() = "Book appointment"]'),
        "search_city_field": (By.XPATH, '//*[@id="input-16"]'),
        "all_cities": (By.XPATH, './/datalist/option'),
        "all_clinics": (By.XPATH, './/button[@name = "facility"]'),
        "all_days": (By.XPATH, './/button[@class = "slds-day"]'),
        "next_month_btn": (By.CSS_SELECTOR, 'button[title="Next Month"]'),
        "all_time_slots": (By.XPATH, './/button[@name="timeslot"]'),
        "appoint_next_btn": (By.XPATH, './/button[text() = "Next"]'),
        "email_checkbox": (By.XPATH, '//*[@id="email-radio-47"]'),
        "email_address": (By.XPATH, './/input[@id = "input-51"]'),
        "confirm_appoint_btn": (By.XPATH, './/button[text() = "Confirm appointment"]'),
        "appointment_confirmed_title": (By.XPATH, './/div[text() = "Appointment Confirmed!"]'),
        "email_text": (By.XPATH, './/div[3]/div[3]/div/div[2]'),
        "refresh_page": (By.XPATH, './/div[@data-tooltip="Refresh"]')
    }

    constants = {
        "link": '/mail/u/'
    }

    def go_to(self):
        base_url = get_setting("URL", "gmail")
        self.browser.get(base_url + self.constants["link"])
        # self.browser.get(base_url)
        try:
            WebDriverWait(self.browser, self.WAIT).until(
                EC.presence_of_element_located(self.locator_dictionary["email_field"]))
        except:
            print("Exception: The user is not navigated to Gamil Login screen.")
        # self.click_element(self.find_element(self.locator_dictionary["atc_btn"]))
        var = self.find_element(self.locator_dictionary["email_field"])
        assert var is not None, "The user is not navigated to G-mail Login screen."

    def login_gmail(self, email_field, password_field, home_screen):
        self.send_text_to_element(self.find_element(self.locator_dictionary["email_field"]), email_field)
        self.click_element(self.find_element(self.locator_dictionary["next_btn"]))

        self.send_text_to_element(self.find_element(self.locator_dictionary["password_field"]), password_field)
        self.click_element(self.find_element(self.locator_dictionary["password_next_btn"]))
        self.is_gmail_login("no")

    def is_gmail_login(self, already_login):
        if already_login == "yes":
            base_url = get_setting("URL", "gmail")
            self.browser.get(base_url + self.constants["link"])
        try:
            WebDriverWait(self.browser, 60).until(
                EC.presence_of_element_located(self.locator_dictionary["search_button"]))
        except:
            print("Exception: The user is not navigated to mail inbox screen.")
        # self.click_element(self.find_element(self.locator_dictionary["atc_btn"]))
        var = self.find_element(self.locator_dictionary["search_button"])
        assert var is not None, "The user is not navigated to mail inbox screen."

    def open_email_click_link(self, search_mail, home_screen, email):
        self.send_text_to_element(self.find_element(self.locator_dictionary["search_mail"]), search_mail)
        self.click_element(self.find_element(self.locator_dictionary["search_button"]))
        for i in range(6):
            try:
                e = WebDriverWait(self.browser, 120).until(
                    EC.presence_of_element_located(self.locator_dictionary[email]))
                break
            except:
                # self.click_element(self.find_element(self.locator_dictionary["refresh_page"]))
                self.browser.refresh()
                print("(Timeout 120): The required link is still not received.")

        self.click_element(self.find_element(self.locator_dictionary[email]))
        if home_screen == "booked":
            email_text = self.get_element_text(self.find_element(self.locator_dictionary["email_text"]))
            return email_text
        else:
            return self.open_click_here_link()

    def open_click_here_link(self):
        # e = WebDriverWait(self.browser, self.WAIT).until(EC.element_to_be_clickable(self.locator_dictionary["click_here_link"]))
        # e.click()
        e = self.find_element(self.locator_dictionary["click_here_link"])
        try:
            e_url = self.get_attribute(e, "href")
        except:
            self.browser.refresh()
            e = self.find_element(self.locator_dictionary["click_here_link"])
            e_url = self.get_attribute(e, "href")
        print("appointment_url = " + e_url)
        time.sleep(2)
        self.browser.get(e_url)
        # self.click_element(e)
        # time.sleep(2)
        # self.browser.switch_to.window(self.browser.window_handles[-1])
        try:
            WebDriverWait(self.browser, 60).until(
                EC.presence_of_element_located(self.locator_dictionary["reg_no_field"]))
        except:
            print("Exception 2: The user is not navigated to appointment main screen.")

        # self.click_element(self.find_element(self.locator_dictionary["atc_btn"]))
        var = self.find_element(self.locator_dictionary["reg_no_field"])
        assert var is not None, "The user is not navigated to appointment main screen."
        return ""

    def enter_booking_no(self, reg_no, phn_no):
        self.send_text_to_element(self.find_element(self.locator_dictionary["reg_no_field"]), reg_no)
        self.send_text_to_element(self.find_element(self.locator_dictionary["phn_number_field"]), phn_no)
        self.click_element(self.find_element(self.locator_dictionary["book_button"]))
        e = None
        try:
            e = WebDriverWait(self.browser, 60).until(
                EC.presence_of_element_located(self.locator_dictionary["search_city_field"]))
        except:
            pass
        assert e is not None, "The user is not navigated to appointment detail screen."

    def enter_booking_details(self):
        e = None
        time.sleep(2)
        # all_cities = len(self.find_elements(self.locator_dictionary["all_cities"]))
        all_cities = 67
        print("all_cities = " + str(all_cities))
        i = 1
        while all_cities >= i:
            option_loc = './/datalist/option[' + str(i) + ']'
            op = self.get_attribute(self.find_element((By.XPATH, option_loc)), 'value')
            print(op)
            self.find_element(self.locator_dictionary["search_city_field"]).clear()
            self.send_text_to_element(self.find_element(self.locator_dictionary["search_city_field"]), op)
            self.click_element(self.find_element((By.XPATH, ".//h1")))
            i = i + 1

            try:
                WebDriverWait(self.browser, self.WAIT).until(
                    EC.presence_of_element_located(self.locator_dictionary["all_clinics"]))
            except:
                continue

            all_clinics = len(self.find_elements(self.locator_dictionary["all_clinics"]))
            j = 1
            while all_clinics >= j:
                option_loc = '(.//button[@name = "facility"])[' + str(j) + ']'
                self.browser.execute_script("arguments[0].scrollIntoView(true);",
                                            self.find_element((By.XPATH, option_loc)))
                self.click_element(self.find_element((By.XPATH, option_loc)))
                j = j + 1
                try:
                    WebDriverWait(self.browser, self.WAIT).until(
                        EC.presence_of_element_located(self.locator_dictionary["all_days"]))
                except:
                    continue

                all_days = len(self.find_elements(self.locator_dictionary["all_days"]))
                k = 1
                while all_days >= k:
                    option_loc = '(.//button[@class = "slds-day"])[' + str(k) + ']'
                    self.browser.execute_script("arguments[0].scrollIntoView(true);",
                                                self.find_element((By.XPATH, option_loc)))
                    self.click_element(self.find_element((By.XPATH, option_loc)))
                    k = k + 1

                    try:
                        WebDriverWait(self.browser, self.WAIT).until(
                            EC.presence_of_element_located(self.locator_dictionary["all_time_slots"]))
                    except:
                        continue

                    all_time_slots = len(self.find_elements(self.locator_dictionary["all_time_slots"]))
                    m = 1
                    while all_time_slots >= m:
                        option_loc = '(.//button[@name="timeslot"])[' + str(m) + ']'
                        self.browser.execute_script("arguments[0].scrollIntoView(true);",
                                                    self.find_element((By.XPATH, option_loc)))
                        self.click_element(self.find_element((By.XPATH, option_loc)))
                        time.sleep(1)
                        m = m + 1

                        self.click_element(self.find_element(self.locator_dictionary["appoint_next_btn"]))
                        time.sleep(2)
                        try:
                            e = WebDriverWait(self.browser, self.WAIT).until(
                                EC.presence_of_element_located(self.locator_dictionary["email_address"]))
                            i = all_cities + 1
                            j = all_clinics + 1
                            k = all_days + 1
                            m = all_time_slots + 1
                            break
                        except:
                            print("Exception: The user is not navigated to appointment confirmation screen. m = " + str(
                                m))
        assert e is not None, "No appointment date/time selected."

    def confirm_booking(self, email_address):
        self.browser.execute_script("arguments[0].scrollIntoView(true);",
                                    self.find_element(self.locator_dictionary["email_checkbox"]))
        self.click_element(self.find_element(self.locator_dictionary["email_checkbox"]))
        self.send_text_to_element(self.find_element(self.locator_dictionary["email_address"]), email_address)
        time.sleep(1)
        self.click_element(self.find_element(self.locator_dictionary["confirm_appoint_btn"]))
        e = None
        try:
            e = WebDriverWait(self.browser, 60).until(
                EC.presence_of_element_located(self.locator_dictionary["appointment_confirmed_title"]))
        except:
            pass
        assert e is not None, "The user is not navigated to appointment success screen."
        time.sleep(10)

    def save_appointment_no(self, first_name):
        base_url = get_setting("URL", "gmail")
        self.browser.get(base_url + self.constants["link"])
        email_text = self.open_email_click_link("\"booking confirmation number is\"", "booked", "booking_email")
        print("email_text = " + email_text)
        if first_name in email_text:
            ap_no = self.extract_no(email_text)
            return ap_no

    def extract_no(self, email_text):
        x = email_text.split("Your  ")[1]
        num = x.split("and bring")[0]
        print(num)
        return num
