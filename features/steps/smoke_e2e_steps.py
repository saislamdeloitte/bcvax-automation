import math
import time
import warnings

from behave import *
import pandas as pd
from pandas.core.common import SettingWithCopyWarning

from pageobjects.BookAppointment import BookAppointment
from pageobjects.CallCenterConsole import CallCenterConsole
from pageobjects.ClinicalProcess import ClinicalPrecess
from pageobjects.Login import Login
from pageobjects.RegisterUser import RegisterUser, get_setting, set_setting


@given("user is on Citizen portal HOME page")
def step_impl(context):
    RegisterUser(context).go_to()


@when('the user clicks "{register_btn}" button, the user is navigated to a {three_steps_form} screen.')
def step_impl(context, register_btn, three_steps_form):
    # RegisterUser(context).click_register_btn(register_btn)
    # df = pd.read_csv('data.csv')
    reg_number = ""
    df = pd.read_csv('data.csv')
    warnings.simplefilter(action="ignore", category=SettingWithCopyWarning)
    total = len(df.index)
    print("Total rows: " + str(total))
    for inc in range(0, total):
        first_name = df['First Name'][inc]
        last_name = df['Last Name'][inc]
        dob = df['DOB'][inc]
        postal = df['POSTAL CODE'][inc]
        phn_number = df['PHN'][inc]
        # i = int(inc)
        print("Row # " + str(inc) + " = " + first_name + " | " + last_name + " | " + dob + " | " + postal + " | " + str(phn_number))
        if not isinstance(df['Reg No'][inc], str):
            if inc != 0:
                RegisterUser(context).go_to()
            RegisterUser(context).click_register_btn(register_btn)
            error_messages = RegisterUser(context).fill_form_step_one(first_name, last_name, dob, postal, str(phn_number))
            if error_messages != "":
                print(error_messages)
                # df[11][inc] = error_messages
                df['Status'][inc] = "Wrong Values"
                df['Errors'][inc] = error_messages
                RegisterUser(context).refresh_browser(df)
                continue
            else:
                RegisterUser(context).fill_form_step_two(get_setting("EMAIL", "email"))
                RegisterUser(context).submit_form("consent_btn", "submit_btn")
                reg = RegisterUser(context).save_reg_number("reg_number")
                df['Reg No'][inc] = reg
                reg_number = reg
                df['Status'][inc] = "Registered"
                df['Errors'][inc] = "No Error!"
                # RegisterUser(context).refresh_browser(df)
        else:
            # RegisterUser(context).refresh_browser(df)
            print("Already registered: " + df['Reg No'][inc])
        df.to_csv('data.csv', index=False)
        # Todo: Call Center Console
        # if not isinstance(df['Status'][inc], str):
        if df['Status'][inc] == "Registered":
            if inc == 0:
                Login(context).go_to()
                Login(context).login_into_website(get_setting("CRED", "call_center_agent"), get_setting("CRED", "password"),
                                                  "Home")
            else:
                Login(context).is_log_in("yes")
            CallCenterConsole(context).check_reg_no(df['Reg No'][inc])
            CallCenterConsole(context).open_patient_record()
            CallCenterConsole(context).check_eligibility("patient", df, 'Status', inc)
            # RegisterUser(context).refresh_browser(df)
        else:
            # RegisterUser(context).refresh_browser(df)
            print("Already registered and eligible: " + df['Reg No'][inc])
        df.to_csv('data.csv', index=False)
        # Todo: Go to email and reg appointment
        if not isinstance(df['Appointment No'][inc], str):
            if inc == 0:
                BookAppointment(context).go_to()
                BookAppointment(context).login_gmail(get_setting("EMAIL", "email"), get_setting("EMAIL", "pw"), "inbox")
            else:
                BookAppointment(context).is_gmail_login("yes")
            BookAppointment(context).open_email_click_link(df['Reg No'][inc], "appointment_home", "first_email")
            BookAppointment(context).enter_booking_no(df['Reg No'][inc], str(phn_number))
            BookAppointment(context).enter_booking_details()
            BookAppointment(context).confirm_booking(get_setting("EMAIL", "email"))
            app_no = BookAppointment(context).save_appointment_no(df['Reg No'][inc])
            df['Appointment No'][inc] = app_no
            df['Status'][inc] = "Booked"
        else:
            # RegisterUser(context).refresh_browser(df)
            print(df['Appointment No'][inc])
        df.to_csv('data.csv', index=False)



@then('the user fills the personal details and click "{continue_btn}" button.')
def step_impl(context, continue_btn):
    # df = pd.read_csv('data.csv')
    df = pd.read_csv('data.csv', header=None)
    total = len(df.index) - 1
    print("Total rows: " + str(total))
    for inc in range(1, total):
        first_name = df[1][inc]
        last_name = df[2][inc]
        dob = df[3][inc]
        postal = df[7][inc]
        phn_number = df[4][inc]
        print("Row # " + str(
            inc) + " = " + first_name + " | " + last_name + " | " + dob + " | " + postal + " | " + phn_number)
        RegisterUser(context).fill_form_step_one(first_name, last_name, dob, postal, phn_number)
        RegisterUser(context).fill_form_step_two(get_setting("EMAIL", "email"))
        RegisterUser(context).submit_form("consent_btn", "submit_btn")


@step('the user enters the "{email}" or "{sms_phone_number}" to send confirmation and click "{continue_btn}" button.')
def step_impl(context, email, sms_phone_number, continue_btn):
    RegisterUser(context).fill_form_step_two(get_setting("EMAIL", "email"))


@then('the user verify all the detail provided, hit "{consent_btn}" button and clicks "{submit_btn}" button to submit '
      'the registration successfully.')
def step_impl(context, consent_btn, submit_btn):
    RegisterUser(context).submit_form(consent_btn, submit_btn)


@step('the user copies "{reg_number}" and saves it.')
def step_impl(context, reg_number):
    reg = RegisterUser(context).save_reg_number(reg_number)
    set_setting("REG_NO", "reg_no", reg)


@given("user is on {user_type} Login Page.")
def step_impl(context, user_type):
    Login(context).go_to()


@when('the user provide the "{user_name}" username and "{password}", and clicks the "{login_btn}" button, the user is '
      'navigated to the "{home_screen}" screen.')
def step_impl(context, user_name, password, login_btn, home_screen):
    Login(context).login_into_website(get_setting("CRED", user_name), get_setting("CRED", "password"), home_screen)


@then("the user search with citizen with his/her registration number and check if the record is there or not.")
def step_impl(context):
    CallCenterConsole(context).check_reg_no(get_setting("REG_NO", "reg_no"))


@step("the clicks and opens the user record.")
def step_impl(context):
    CallCenterConsole(context).open_patient_record()


@then('the user clicks on the "{check_eligibility_btn}" button, selects the "{vaccination_option}" option and check '
      'if the {user_type} is eligible.')
def step_impl(context, check_eligibility_btn, vaccination_option, user_type):
    CallCenterConsole(context).check_eligibility(user_type, "", "", "")


@when('the user clicks "{reg_btn}" button, the user is navigated to 3 step form screen.')
def step_impl(context, reg_btn):
    CallCenterConsole(context).click_register_btn(reg_btn)


@then('the user fills the personal details, verify "{phn_number}" number and clicks "{next_btn}" button.')
def step_impl(context, phn_number, next_btn):
    df = pd.read_csv('data.csv')
    warnings.simplefilter(action="ignore", category=SettingWithCopyWarning)
    total = len(df.index)
    print("Total rows: " + str(total))
    for inc in range(0, 1):
        first_name = df['First Name'][inc]
        last_name = df['Last Name'][inc]
        dob = df['DOB'][inc]
        postal = df['POSTAL CODE'][inc]
        phn_number = df['PHN'][inc]
        CallCenterConsole(context).fill_form_step_one(first_name, last_name, dob, postal, str(phn_number))


@step('the user enters the "{email}" or "{sms_phone_number}" to send confirmation email and click "{review_btn}" '
      'button.')
def step_impl(context, email, sms_phone_number, review_btn):
    CallCenterConsole(context).fill_form_step_two(get_setting("EMAIL", "email"))


@then('the user verify all the details provided, and clicks "{register_btn}" button to submit the registration '
      'successfully.')
def step_impl(context, register_btn):
    CallCenterConsole(context).submit_form(register_btn)


@step("if the user is eligible, go the {appointment} tab of the citizen list screen.")
def step_impl(context, appointment):
    CallCenterConsole(context).go_to_appointment_tab()


@when("the user selects {vaccine}, {city} and {hospital}, and click {select} button on available slots.")
def step_impl(context, vaccine, city, hospital, select):
    CallCenterConsole(context).search_and_select_appointment()


@then("the user {saves} the appointment after reviewing.")
def step_impl(context, saves):
    CallCenterConsole(context).save_appointment()


@step("clicks the provides {sgi_number} number to go to the {appointment_list} screen to verify the {"
      "appointment_confirmation_number}.")
def step_impl(context, sgi_number, appointment_list, appointment_confirmation_number):
    CallCenterConsole(context).get_appointment_number()


@given("user is on g-mail {login_page} Page")
def step_impl(context, login_page):
    BookAppointment(context).go_to()


@when('the user provide the "{user_name}" and "{password}", and clicks the "{login_btn}" button, the user is '
      'navigated to the mail "{inbox}" screen.')
def step_impl(context, user_name, password, login_btn, inbox):
    BookAppointment(context).login_gmail(get_setting("EMAIL", "email"), get_setting("EMAIL", "pw"), inbox)


@then("the user opens the received email and click the link {click_here_link} which will take the user to the {"
      "appointment} screen.")
def step_impl(context, click_here_link, appointment):
    BookAppointment(context).open_email_click_link("\"- Confirmation number " + get_setting("REG_NO", "reg_no") + "\"",
                                                   appointment, "first_email")


@when("the user clicks {book_appointment} button after entering {registration_confirmation_number} and {phn_number}, "
      "the user is moved to appointment detail screen.")
def step_impl(context, book_appointment, registration_confirmation_number, phn_number):
    BookAppointment(context).enter_booking_no(get_setting("REG_NO", "reg_no"), '9879454689')


@step("the user selects the {appointment_details} and clicks {next_btn} button.")
def step_impl(context, appointment_details, next_btn):
    BookAppointment(context).enter_booking_details()


@then("the user enters email and confirm booking.")
def step_impl(context):
    BookAppointment(context).confirm_booking(get_setting("EMAIL", "email"))


@then("close the browser for {user_type}.")
def step_impl(context, user_type):
    Login(context).close_browser(user_type)


@step("the user navigates to the {module_name} module.")
def step_impl(context, module_name):
    ClinicalPrecess(context).move_to(module_name)


@then("the user search the citizen with patient registration number.")
def step_impl(context):
    ClinicalPrecess(context).search_citizen(get_setting("REG_NO", "reg_no"))


@when("the user {view_btn} the searched record, he will be navigated to {process_screen} screen.")
def step_impl(context, view_btn, process_screen):
    ClinicalPrecess(context).view_record(process_screen)


@then("the user edits the user information and submit the updates.")
def step_impl(context):
    ClinicalPrecess(context).edit_record()


@step("clicks on the {rebook_btn} button to schedule the appointment in the clinic.")
def step_impl(context, rebook_btn):
    ClinicalPrecess(context).rebook_appointment(rebook_btn)


@then("the user clicks the {confirm_button} button to update the information successfully.")
def step_impl(context, confirm_button):
    ClinicalPrecess(context).confirm_step()


@when("the user view the searched record, he will be navigated to {process_screen} screen again.")
def step_impl(context, process_screen):
    ClinicalPrecess(context).view_record(process_screen)
